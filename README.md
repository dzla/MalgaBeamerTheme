# How to install

Clone this repository somewhere where beamer will look for it. This could typically be `~/texmf/tex/latex/local/` on linux (create the full path if it does not exist). 

# How to use

To use the theme: `\usetheme[options]{malga}`

For a 16/9 format, use beamer's `aspectratio` option : `\documentclass[aspectratio=169]{beamer}`.

Slides with inverted background (e.g. for sections' titles) can be created with `\begin{frame}[standout] … \end{frame}`.

# Customization

List of possible options:
- `logo=everyslide` to add a unige/malga logo in the bottom left of every slide.
- `frontpagelogo=none` to remove the logo on the title slide (possible options: `none` / `dibris`)
- `erclogo` (possible options: `erc`(default) / `none` / `corner`)

If you want to add additional options, let me know. Or even better, just do it. Pull requests are welcome!
